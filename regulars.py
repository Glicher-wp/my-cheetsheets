"""
This module contains useful regular expressions templates.
"""
import re

# Regulars compile for all smiles including colors and composite (like 3 smiles in 1).
EMOJI_PATTERN = re.compile(
    ".\U0000FE0F|" #Символьные смайлы
    "\U0000200D|" #Склеивающий символ
    "["
    "\U0001F1E0-\U0001F1FF"  # Flags (iOS)
    "\U0001F300-\U0001F3FA"  # Symbols & pictographs
    "\U0001F400-\U0001F5FF"  # Symbols & pictographs
    "\U0001F600-\U0001F64F"  # Emoticons
    "\U0001F680-\U0001F6FF"  # Transport & map symbols
    "\U0001F700-\U0001F77F"  # Alchemical symbols
    "\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
    "\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
    "\U0001F900-\U0001F9AF"  # Supplemental Symbols and Pictographs
    "\U0001F9B4-\U0001F9FF"  # Supplemental Symbols and Pictographs
    "\U0001FA00-\U0001FA6F"  # Chess Symbols
    "\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
    "\U000024C2-\U0001F251"
    "]"
    "["
    "\U0001F3FB-\U0001F3FF" #Цвета кожи
    "\U0001F9B0-\U0001F9B3" #Цвета волос
    "\U0001F466-\U0001F469" #Пол 
    "]*"
)
-- Delete all duplicate values in table in a fast way.
DELETE FROM table_name a USING (
      SELECT MIN(ctid) as ctid, key
        FROM table_name
        GROUP BY key HAVING COUNT(*) > 1
      ) b
      WHERE a.key = b.key
      AND a.ctid <> b.ctid
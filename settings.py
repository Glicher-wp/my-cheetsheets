"""
This module contains usefull settings params for difference frameworks.
"""

import seaborn as sns


# Preset for pretty graphs.
sns.set(
    rc={
        'figure.figsize': (16, 9),
        'figure.dpi': 80,
        'axes.grid': True,
        'axes.grid.axis': 'y',
        'axes.grid.which': 'both',
        'grid.alpha': .4,
        'ytick.minor.visible': True,
        'xtick.minor.visible': True,
        },
    palette='colorblind',
    style='ticks'
)
